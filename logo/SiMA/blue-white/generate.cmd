set inkscape="%ProgramFiles%\Inkscape\inkscape.exe"
%inkscape% --export-png "SiMA logo blue-white.png" --export-area-drawing --export-width 700 --export-height 700 --export-background "white" "SiMA logo blue-white.svg"
%inkscape% --export-pdf "SiMA logo blue-white.pdf" --export-area-drawing "SiMA logo blue-white.svg"

copy ..\..\other\dmmConvertPNGtoJPG.scm %USERPROFILE%\.gimp-2.8\scripts\
set gimp="%ProgramFiles%\GIMP 2\bin\gimp-2.8.exe"
%gimp% --console-messages --no-interface --no-data --batch="(dmmConvertPNGtoJPG \"SiMA logo blue-white.png\" \"SiMA logo blue-white.jpg\")" --batch="(gimp-quit 0)"
