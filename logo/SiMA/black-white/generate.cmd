set inkscape="%ProgramFiles%\Inkscape\inkscape.exe"
%inkscape% --export-png "SiMA logo bw.png" --export-area-drawing --export-width 700 --export-height 700 --export-background "white" "SiMA logo bw.svg"
%inkscape% --export-pdf "SiMA logo bw.pdf" --export-area-drawing "SiMA logo bw.svg"

copy ..\..\other\dmmConvertPNGtoJPG.scm %USERPROFILE%\.gimp-2.8\scripts\
set gimp="%ProgramFiles%\GIMP 2\bin\gimp-2.8.exe"
%gimp% --console-messages --no-interface --no-data --batch="(dmmConvertPNGtoJPG \"SiMA logo bw.png\" \"SiMA logo bw.jpg\")" --batch="(gimp-quit 0)"
