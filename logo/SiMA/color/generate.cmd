set inkscape="%ProgramFiles%\Inkscape\inkscape.exe"
%inkscape% --export-png "SiMA logo.png" --export-area-drawing --export-width 700 --export-height 700 --export-background "white" "SiMA logo.svg"
%inkscape% --export-png "SiMA logo small for web.png" --export-area-drawing --export-width 200 --export-height 200 --export-background "white" "SiMA logo.svg"
%inkscape% --export-pdf "SiMA logo.pdf" --export-area-drawing "SiMA logo.svg"

copy ..\..\other\dmmConvertPNGtoJPG.scm %USERPROFILE%\.gimp-2.8\scripts\
set gimp="%ProgramFiles%\GIMP 2\bin\gimp-2.8.exe"
%gimp% --console-messages --no-interface --no-data --batch="(dmmConvertPNGtoJPG \"SiMA logo.png\" \"SiMA logo.jpg\")" --batch="(gimp-quit 0)"
%gimp% --console-messages --no-interface --no-data --batch="(dmmConvertPNGtoJPG \"SiMA logo small for web.png\" \"SiMA logo small for web.jpg\")" --batch="(gimp-quit 0)"
