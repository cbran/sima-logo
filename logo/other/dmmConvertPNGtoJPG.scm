; dmmConvertPNGtoJPG.scm - GIMP Script-Fu to Convert PNG to JPG
;    This Script-Fu must be put in The GIMP's script directory
;    (e.g., $HOME/.gimp-1.2/scripts).
;    For command-line invocation, use the shell script dmmConvertPNGtoJPG.sh
;    For interactive invocation, run The GIMP and go to
;    Xtns -> Script-Fu -> dmm
;
(define (dmmConvertPNGtoJPG infile outfile)
   (let* ((image (car (file-png-load 1 infile infile)))
          (drawable (car (gimp-image-active-drawable image)))
         )

         (file-jpeg-save 1 image drawable outfile outfile 
              0.99 0 1 1 "http://sima.ict.tuwien.ac.at/" 0 1 0 0 )
            ; 0.99 quality (float 0 <= x <= 1)
            ;      0 smoothing factor (0 <= x <= 1)
            ;        1 optimization of entropy encoding parameter (0/1)
            ;          1 enable progressive jpeg image loading (0/1)
            ;            "xxxx"  image comment
            ;                   0 subsampling option number
            ;                     1 force creation of a baseline JPEG
            ;                       0 frequency of restart markers 
            ;                         in rows, 0 = no restart markers
            ;                         0 DCT algoritm to use 
   )
)

(script-fu-register                                 ; I always forget these ...
   "dmmConvertPNGtoJPG"                             ; script name to register
   "<Toolbox>/Xtns/Script-Fu/dmm/dmmConvertPNGtoJPG" ; where it goes
   "dmm Convert PNG to JPG"                         ; script description
   "David M. MacMillan"                             ; author
   "Copyright 2004 by David M. MacMillan; GNU GPL"  ; copyright
   "2004-01-27"                                     ; date
   ""                                               ; type of image
   SF-FILENAME "Infile" "infile.png"                ; default parameters
   SF-FILENAME "Outfile" "outfile.png"
)